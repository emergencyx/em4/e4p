# e4p - Emergency 4 prototype reader and writer
## Installation
- `yarn install @emergencyx/e4p`

## Usage
Parse a prototype
```
    const fs = require('fs');
    const e4p = require('@emergencyx/e4p'); // Import e4p

    buffer = fs.readFileSync('file.e4p');
    prototype = e4p.parse(buffer); // Returns decoded prototype
```
    
Write a prototype
```
    const fs = require('fs');
    const e4p = require('@emergencyx/e4p'); // Import e4p
    ...
    buffer = e4p.serialize(prototype); // Convert prototype to .e4p buffer
```

## Testing
- `yarn test`

Parses and serializes .e4p files placed in `test/input/`.

Test data must be compressed before committing: `tar -zcf input.tar.gz test/input`.
Take care not to accidentally include any other files, e.g. test/test.js.

## Kaitai Description
`emergency4_prototype.ksy` is a YAML file that describes the Emergency4 .e4p file format for the [Kaitai Compiler](https://kaitai.io/#quick-start).
It is language neutral and allows you to generate parser code in any programming language supported by Kaitai.
A pre-generated version for JavaScript can be found in the `generated/` directory.
Note that Kaitai provides only a parser at this time, not a serializer.
