meta:
  id: emergency4_prototype
  title: "Binary Prototype Format"
  application: "Emergency 4"
  file-extension: .e4p
  endian: le
seq:
  - id: path
    type: em4_string_type
  - id: magic
    contents: [0x01, 0x00, 0x00, 0x00]
  - id: root
    type: em4_element
types:
  em4_string_type:
    doc: |
      Strings are prefixed with their length in characters and encoded
      using UTF-16, therefore every character is stored in two bytes.
    seq:
      - id: len
        type: u4
      - id: value
        type: str
        encoding: UTF-16LE
        size: len * 2
  em4_element:
    seq:
      - id: magic
        contents: [0x01, 0x00, 0x00, 0x00]
      - id: name
        type: em4_string_type
      - id: child_count
        type: u4
      - id: children
        type: em4_element
        repeat: expr
        repeat-expr: child_count
      - id: attribute_count
        type: u4
      - id: attributes
        type: em4_attribute
        repeat: expr
        repeat-expr: attribute_count
  em4_attribute:
    seq:
      - id: key
        type: em4_string_type
      - id: value_type
        type: u4
      - id: value
        type:
          switch-on: value_type
          cases:
            1: s4
            2: f4
            4: em4_string_type
