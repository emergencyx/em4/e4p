// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Emergency4Prototype = factory(root.KaitaiStream);
  }
}(this, function (KaitaiStream) {
var Emergency4Prototype = (function() {
  function Emergency4Prototype(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Emergency4Prototype.prototype._read = function() {
    this.path = new Em4StringType(this._io, this, this._root);
    this.magic = this._io.readBytes(4);
    if (!((KaitaiStream.byteArrayCompare(this.magic, [1, 0, 0, 0]) == 0))) {
      throw new KaitaiStream.ValidationNotEqualError([1, 0, 0, 0], this.magic, this._io, "/seq/1");
    }
    this.root = new Em4Element(this._io, this, this._root);
  }

  /**
   * Strings are prefixed with their length in characters and encoded
   * using UTF-16, therefore every character is stored in two bytes.
   */

  var Em4StringType = Emergency4Prototype.Em4StringType = (function() {
    function Em4StringType(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Em4StringType.prototype._read = function() {
      this.len = this._io.readU4le();
      this.value = KaitaiStream.bytesToStr(this._io.readBytes((this.len * 2)), "UTF-16LE");
    }

    return Em4StringType;
  })();

  var Em4Element = Emergency4Prototype.Em4Element = (function() {
    function Em4Element(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Em4Element.prototype._read = function() {
      this.magic = this._io.readBytes(4);
      if (!((KaitaiStream.byteArrayCompare(this.magic, [1, 0, 0, 0]) == 0))) {
        throw new KaitaiStream.ValidationNotEqualError([1, 0, 0, 0], this.magic, this._io, "/types/em4_element/seq/0");
      }
      this.name = new Em4StringType(this._io, this, this._root);
      this.childCount = this._io.readU4le();
      this.children = new Array(this.childCount);
      for (var i = 0; i < this.childCount; i++) {
        this.children[i] = new Em4Element(this._io, this, this._root);
      }
      this.attributeCount = this._io.readU4le();
      this.attributes = new Array(this.attributeCount);
      for (var i = 0; i < this.attributeCount; i++) {
        this.attributes[i] = new Em4Attribute(this._io, this, this._root);
      }
    }

    return Em4Element;
  })();

  var Em4Attribute = Emergency4Prototype.Em4Attribute = (function() {
    function Em4Attribute(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Em4Attribute.prototype._read = function() {
      this.key = new Em4StringType(this._io, this, this._root);
      this.valueType = this._io.readU4le();
      switch (this.valueType) {
      case 1:
        this.value = this._io.readS4le();
        break;
      case 2:
        this.value = this._io.readF4le();
        break;
      case 4:
        this.value = new Em4StringType(this._io, this, this._root);
        break;
      }
    }

    return Em4Attribute;
  })();

  return Emergency4Prototype;
})();
return Emergency4Prototype;
}));
