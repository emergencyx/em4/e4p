const parser = require('./src/parser');
const serializer = require('./src/serializer');

module.exports = {
    parse: parser.parse,
    serialize: serializer.serialize
};