const fs = require('fs');
const path = require('path');
const assert = require('assert');
const e4p = require('../index');
const serialize = require('../src/serializer');

const inputDirectory = path.join(__dirname, 'input/');

describe('Parser should process .e4p files properly', function () {
    for (file of fs.readdirSync(inputDirectory)) {
        it(`should match the original binary of ${file}`, function () {
            const expected = fs.readFileSync(`${inputDirectory}/${file}`);
            const prototype = e4p.parse(expected);
            const actual = e4p.serialize(prototype);
            assert.ok(expected.equals(actual), 'Buffer must match');
        });
    }
});

describe('Parser should write .e4p properly', function () {
    it('should serialize em4 prototype strings', function () {
        const input = 'Children';
        const expected = Buffer.from('080000004300680069006C006400720065006E00', 'hex');
        const actual = serialize.em4String(input);
        assert.ok(expected.equals(actual));
    });

    describe('should serialize attributes', function () {
        it('should serialize type 1 attributes (int or uint)', function () {
            const input = { 'key': 'body', 'type': 1, 'value': 0 };
            const expected = Buffer.from('0400000062006F00640079000100000000000000', 'hex');
            const actual = serialize.attribute(input);
            assert.ok(expected.equals(actual), 'Buffer should match');
        });

        it('should serialize type 2 attributes (float)', function () {
            const input = { 'key': 'bounce', 'type': 2, 'value': 0.20000000298023224 };
            const expected = Buffer.from('0600000062006F0075006E006300650002000000CDCC4C3E', 'hex');
            const actual = serialize.attribute(input);
            assert.ok(expected.equals(actual), 'Buffer should match');
        });

        it('should serialize type 4 attributes (string)', function () {
            const input = { 'key': 'texture', 'type': 4, 'value': 'rectangle2.tga' };
            const expected = Buffer.from('070000007400650078007400750072006500040000000E000000720065006300740061006E0067006C00650032002E00740067006100', 'hex');
            const actual = serialize.attribute(input);
            assert.ok(expected.equals(actual), 'Buffer should match');
        });
    });
});
